import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseService } from 'src/base';
import { SubCategory } from './sub-category.entity';
import { Repository } from 'typeorm';

@Injectable()
export class SubCategoryService extends BaseService<SubCategory> {
  constructor(
    @InjectRepository(SubCategory)
    private subcategorRepository: Repository<SubCategory>,
  ) {
    super(subcategorRepository);
  }
}
