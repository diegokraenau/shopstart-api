import { Controller } from '@nestjs/common';
import { BaseController } from 'src/base';
import { Taxonomy } from './taxonomy.entity';
import { TaxonomyService } from './taxonomy.service';

@Controller('taxonomies')
export class TaxonomyController extends BaseController<Taxonomy> {
  constructor(private taxonomyService: TaxonomyService) {
    super(taxonomyService);
  }
}
