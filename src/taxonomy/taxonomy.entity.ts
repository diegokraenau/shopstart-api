import { SubCategory } from './../sub-category/sub-category.entity';
import { Product } from '../product/product.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('taxonomies')
export class Taxonomy {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ name: 'name', type: 'varchar' })
  name: string;

  @ManyToOne(() => SubCategory, (subcategory) => subcategory.taxonomies, {
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'subcategory_id' })
  subCategory: SubCategory;

  @OneToMany(() => Product, (product) => product.taxonomy)
  products: Product[];
}
