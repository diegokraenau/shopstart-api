import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { BaseController } from 'src/base';
import { Product } from './product.entity';
import { ProductService } from './product.service';

@Controller('products')
export class ProductController extends BaseController<Product> {
  constructor(private productService: ProductService) {
    super(productService);
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    return await this.productService.findOneWithRelations(id, [
      'taxonomy.subCategory.category',
    ]);
  }

  @Get()
  async getAll() {
    return await this.productService.getAllWithRelations([
      'taxonomy.subCategory.category',
    ]);
  }

  @Post('calculate')
  async calculate(@Body() data: any) {
    return await this.productService.calculate(data.amount);
  }
}
