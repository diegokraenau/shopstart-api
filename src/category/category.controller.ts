import { Controller, Get } from '@nestjs/common';
import { BaseController } from 'src/base';
import { CategoryService } from './category.service';
import { Category } from './category.entity';

@Controller('categories')
export class CategoryController extends BaseController<Category> {
  constructor(private categoryService: CategoryService) {
    super(categoryService);
  }

  @Get()
  async getAll() {
    return this.categoryService.getAllWithRelations([
      'subcategories.taxonomies',
    ]);
  }
}
