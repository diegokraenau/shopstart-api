import { Repository } from 'typeorm';
import { BaseEntity } from './base.entity';

export class BaseService<T extends BaseEntity> {
  constructor(private entityRepository: Repository<T>) {}

  async getAll() {
    return await this.entityRepository.find({});
  }

  async createOne(entity: any) {
    return await this.entityRepository.save(entity);
  }

  async findOne(id: any) {
    return await this.entityRepository.findOneBy({ id });
  }

  async deleteOne(id: any) {
    return await this.entityRepository.delete({ id });
  }

  async updateOne(id: any, entity: any) {
    return await this.entityRepository.update({ id }, entity);
  }

  async getAllWithRelations(relations: string[]) {
    return await this.entityRepository.find({
      relations,
    });
  }

  async findOneWithRelations(id: any, relations: string[]) {
    return await this.entityRepository.findOne({
      where: {
        id,
      },
      relations,
    });
  }
}
