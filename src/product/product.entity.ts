import { Taxonomy } from './../taxonomy/taxonomy.entity';

import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('products')
export class Product {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ name: 'name', type: 'varchar' })
  name: string;

  @Column({ name: 'description', type: 'varchar' })
  description: string;

  @Column({ name: 'price', type: 'float' })
  price: number;

  @Column({ name: 'image_coverted', type: 'text' })
  imageConverted: string;

  @ManyToOne(() => Taxonomy, (taxonomy) => taxonomy.products, {
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'taxonomy_id' })
  taxonomy: Taxonomy;
}
