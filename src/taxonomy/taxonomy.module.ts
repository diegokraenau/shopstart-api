import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TaxonomyController } from './taxonomy.controller';
import { Taxonomy } from './taxonomy.entity';
import { TaxonomyService } from './taxonomy.service';

@Module({
  imports: [TypeOrmModule.forFeature([Taxonomy])],
  controllers: [TaxonomyController],
  providers: [TaxonomyService],
})
export class TaxonomyModule {}
