import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { SubCategory } from '../sub-category/sub-category.entity';

@Entity('categories')
export class Category {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ name: 'name', type: 'varchar' })
  name: string;

  @OneToMany(() => SubCategory, (subCategory) => subCategory.category)
  subcategories: SubCategory[];
}
