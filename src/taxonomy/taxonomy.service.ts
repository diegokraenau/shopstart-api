import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseService } from 'src/base';
import { Taxonomy } from './taxonomy.entity';
import { Repository } from 'typeorm';

@Injectable()
export class TaxonomyService extends BaseService<Taxonomy> {
  constructor(
    @InjectRepository(Taxonomy)
    private taxonomyRepository: Repository<Taxonomy>,
  ) {
    super(taxonomyRepository);
  }
}
