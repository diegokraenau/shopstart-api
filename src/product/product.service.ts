import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseService } from 'src/base';
import { Product } from './product.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ProductService extends BaseService<Product> {
  constructor(
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
  ) {
    super(productRepository);
  }

  async calculate(amount: number) {
    const amountsPerMonth = [];
    const months = [6, 12, 18, 24];

    return months.map((x) => {
      const response = this.formula(amount, x);
      return {
        month: x,
        amount: response,
      };
    });
  }

  private formula(amount: number, mounts: number) {
    const result =
      amount * Math.pow((1 - Math.pow(1 + 0.01877, -mounts)) / 0.01877, -1);

    return result;
  }
}
