import { Controller, Get } from '@nestjs/common';
import { BaseController } from 'src/base';
import { SubCategory } from './sub-category.entity';
import { SubCategoryService } from './sub-category.service';

@Controller('sub-categories')
export class SubCategoryController extends BaseController<SubCategory> {
  constructor(private subcategoryService: SubCategoryService) {
    super(subcategoryService);
  }
}
